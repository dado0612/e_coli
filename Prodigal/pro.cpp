#include <bits/stdc++.h>
#include <dirent.h>
using namespace std;

string src_path = "/home/dado/Downloads/e_coli/Genome";
string dst_path = "/home/dado/Downloads/Prodigal";
string src_file = ".fsa_nt.gz";
string dst_file = ".faa";

int main() {
	int cnt = 0;
	struct dirent *entry;
	DIR *root = opendir(src_path.c_str());

	while ((entry = readdir(root))) {
		string folder = string(entry->d_name);
		string path = src_path + "/" + folder;

		struct dirent *ent;
		DIR *dir = opendir(path.c_str());

		while ((ent = readdir(dir))) {
			string file = string(ent->d_name);
			if (file.find(src_file) != string::npos) {
				file = path + "/" + file;
				folder = dst_path + "/" + folder + dst_file;

				cout << "No: " << ++cnt << endl;
				string cmd = "prodigal -i " + file + " -o my.genes -a " + folder
						+ " -p meta";
				system(cmd.c_str());
			}
		}
	}

	cout << "Total: " << cnt << endl;
	return 0;
}
